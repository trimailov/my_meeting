import pytest
from rest_framework.test import APIClient

from tests.factories import TokenFactory


@pytest.fixture
def user_token():
    return TokenFactory()


@pytest.fixture
def client():
    return APIClient()


@pytest.fixture
def auth_client(user_token):
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION="Token " + user_token.key)
    return client
