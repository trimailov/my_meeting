import datetime

import factory
from django.utils import timezone
from rest_framework.authtoken.models import Token

from app.models import Employee, MeetingRoom, Reservation


class EmployeeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Employee

    username = factory.Sequence(lambda n: "user%d" % n)
    email = factory.Sequence(lambda n: "user%d@example.com" % n)
    password = factory.PostGenerationMethodCall("set_password", "letmetest")


class TokenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Token

    user = factory.SubFactory(EmployeeFactory)


class MeetingRoomFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MeetingRoom

    name = factory.Sequence(lambda n: f"MR #{n}")


class ReservationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Reservation

    title = factory.Sequence(lambda n: f"Reservation #{n}")
    from_dt = timezone.now()
    until_dt = timezone.now() + datetime.timedelta(hours=1)
    created_by = factory.SubFactory(EmployeeFactory)
    meeting_room = factory.SubFactory(MeetingRoomFactory)
    canceled = False

    @factory.post_generation
    def employees(self, create, extracted, **kwargs):
        if not extracted:
            # on build() called - do nothing
            return
        if extracted:
            for employee in extracted:
                self.employees.add(employee)
