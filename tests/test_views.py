import datetime

import pytest
from django.urls import reverse

from app.models import MeetingRoom, Reservation
from tests.factories import (
    EmployeeFactory,
    MeetingRoomFactory,
    ReservationFactory,
)

pytestmark = pytest.mark.django_db


def test_get_meeting_rooms_no_auth(client):
    MeetingRoomFactory()
    MeetingRoomFactory()
    resp = client.get(reverse("app:meetingroom-list"))
    assert resp.status_code == 401


def test_get_meeting_rooms(auth_client):
    MeetingRoomFactory()
    MeetingRoomFactory()

    resp = auth_client.get(reverse("app:meetingroom-list"))
    assert resp.status_code == 200
    assert resp.json()["count"] == 2


def test_create_meeting_rooms_no_auth(client):
    resp = client.post(reverse("app:meetingroom-list"), {"name": "foo"})
    assert resp.status_code == 401


def test_create_meeting_rooms(auth_client):
    resp = auth_client.post(reverse("app:meetingroom-list"), {"name": "foo"})
    assert resp.status_code == 201
    assert MeetingRoom.objects.count() == 1
    data = resp.json()
    assert data == {
        "uuid": data["uuid"],
        "name": "foo",
    }


def test_create_meeting_rooms_blank_name(auth_client):
    resp = auth_client.post(reverse("app:meetingroom-list"), {"name": ""})
    assert resp.status_code == 400
    assert list(resp.json().keys()) == ["name"]
    assert MeetingRoom.objects.count() == 0


def test_create_meeting_rooms_no_data(auth_client):
    resp = auth_client.post(reverse("app:meetingroom-list"))
    assert resp.status_code == 400
    assert list(resp.json().keys()) == ["name"]
    assert MeetingRoom.objects.count() == 0


def test_get_reservations_no_auth(client):
    ReservationFactory()
    resp = client.get(reverse("app:reservation-list"))
    assert resp.status_code == 401


def test_get_reservations(auth_client):
    ReservationFactory()
    resp = auth_client.get(reverse("app:reservation-list"))
    assert resp.status_code == 200
    assert resp.json()["count"] == 1


def test_create_reservation_no_auth(client):
    resp = client.post(reverse("app:reservation-list"), {"title": "sprint planning"})
    assert resp.status_code == 401


def test_create_reservation(auth_client):
    m1 = MeetingRoomFactory()
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    resp = auth_client.post(
        reverse("app:reservation-list"),
        {
            "title": "sprint planning",
            "from_dt": datetime.datetime(2020, 3, 14, hour=15, minute=45),
            "until_dt": datetime.datetime(2020, 3, 14, hour=16, minute=15),
            "meeting_room": m1.uuid,
            "employees": [e1.uuid, e2.uuid],
        },
    )
    assert resp.status_code == 201
    data = resp.json()
    # Check response data to make sure that foreign keys are set, including
    # implicit `created_by` key.
    assert data == {
        "uuid": data["uuid"],
        "title": "sprint planning",
        "from_dt": "2020-03-14T15:45:00Z",
        "until_dt": "2020-03-14T16:15:00Z",
        "meeting_room": str(m1.uuid),
        "created_by": data["created_by"],
        "employees": [str(e1.uuid), str(e2.uuid)],
        "canceled": False,
    }


def test_cancel_reservation_no_auth(client):
    r1 = ReservationFactory()
    resp = client.patch(reverse("app:reservation-cancel", kwargs={"pk": r1.uuid}))
    assert resp.status_code == 401


def test_cancel_reservation(auth_client):
    r1 = ReservationFactory()
    resp = auth_client.patch(reverse("app:reservation-cancel", kwargs={"pk": r1.uuid}))
    assert resp.status_code == 200
    assert resp.json()["canceled"] is True
    # sanity check that Reservation is indeed canceled
    assert Reservation.objects.get(pk=r1.pk).canceled is True


def test_filter_reservation_by_employee(auth_client):
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    e3 = EmployeeFactory()
    r1 = ReservationFactory.create(employees=(e1, e2))
    _ = ReservationFactory.create(employees=(e1, e3))
    r3 = ReservationFactory.create(employees=(e2, e3))

    # filter reservations for employee #2, which should return reservations #1 and #3
    resp = auth_client.get(reverse("app:reservation-list") + f"?employees={e2.uuid}")
    assert resp.status_code == 200

    data = resp.json()
    assert data["count"] == 2

    # check id's of the returned json, bit ugly, but it does the job,
    # probably should need some default sorting or sorting in filter
    sorted_results = sorted(data["results"], key=lambda x: x["uuid"])
    assert list(map(lambda x: x["uuid"], sorted_results)) == sorted(
        [str(r1.uuid), str(r3.uuid)]
    )


def test_filter_reservation_by_employee_empty(auth_client):
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    e3 = EmployeeFactory()
    ReservationFactory.create(employees=(e1, e2))
    ReservationFactory.create(employees=(e1,))

    # filter reservations for employee #3, which should return no reservations
    resp = auth_client.get(reverse("app:reservation-list") + f"?employees={e3.uuid}")
    assert resp.status_code == 200

    data = resp.json()
    assert data["count"] == 0


def test_filter_reservation_by_multiple_employees(auth_client):
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    e3 = EmployeeFactory()
    r1 = ReservationFactory.create(employees=(e1, e2))
    r2 = ReservationFactory.create(employees=(e1, e3))
    r3 = ReservationFactory.create(employees=(e2, e3))
    _ = ReservationFactory.create(employees=(e1,))

    # filter reservations for employees #2 and #3, which should return reservations #1, #2 and #3
    resp = auth_client.get(
        reverse("app:reservation-list") + f"?employees={e2.uuid}&employees={e3.uuid}"
    )
    assert resp.status_code == 200

    data = resp.json()
    assert data["count"] == 3

    # check id's of the returned json, bit ugly, but it does the job,
    # probably should need some default sorting or sorting in filter
    sorted_results = sorted(data["results"], key=lambda x: x["uuid"])
    assert list(map(lambda x: x["uuid"], sorted_results)) == sorted(
        [str(r1.uuid), str(r2.uuid), str(r3.uuid)]
    )


def test_filter_reservation_by_multiple_usernames(auth_client):
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    e3 = EmployeeFactory()
    r1 = ReservationFactory.create(employees=(e1, e2))
    r2 = ReservationFactory.create(employees=(e1, e3))
    r3 = ReservationFactory.create(employees=(e2, e3))
    _ = ReservationFactory.create(employees=(e1,))

    # filter reservations for employees #2 and #3, which should return reservations #1, #2 and #3
    resp = auth_client.get(
        reverse("app:reservation-list")
        + f"?usernames={e2.username}&usernames={e3.username}"
    )
    assert resp.status_code == 200

    data = resp.json()
    assert data["count"] == 3

    # check id's of the returned json, bit ugly, but it does the job,
    # probably should need some default sorting or sorting in filter
    sorted_results = sorted(data["results"], key=lambda x: x["uuid"])
    assert list(map(lambda x: x["uuid"], sorted_results)) == sorted(
        [str(r1.uuid), str(r2.uuid), str(r3.uuid)]
    )


def test_exclude_canceled_reservation(auth_client):
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    e3 = EmployeeFactory()
    r1 = ReservationFactory.create(employees=(e1, e2))
    r2 = ReservationFactory.create(employees=(e1, e3))
    _ = ReservationFactory.create(employees=(e2, e3), canceled=True)

    # filter reservations for employee #2, which should return reservations #1 and #3
    resp = auth_client.get(reverse("app:reservation-list") + "?canceled=false")
    assert resp.status_code == 200

    data = resp.json()
    assert data["count"] == 2

    # check id's of the returned json, bit ugly, but it does the job,
    # probably should need some default sorting or sorting in filter
    sorted_results = sorted(data["results"], key=lambda x: x["uuid"])
    assert list(map(lambda x: x["uuid"], sorted_results)) == sorted(
        [str(r1.uuid), str(r2.uuid)]
    )


def test_filter_reservation_by_meeting_room(auth_client):
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    e3 = EmployeeFactory()
    m1 = MeetingRoomFactory()
    m2 = MeetingRoomFactory()
    r1 = ReservationFactory.create(employees=(e1, e2), meeting_room=m1)
    _ = ReservationFactory.create(employees=(e1, e3), meeting_room=m2)
    r3 = ReservationFactory.create(employees=(e2, e3), meeting_room=m1)

    # filter reservations for employee #2, which should return reservations #1 and #3
    resp = auth_client.get(reverse("app:reservation-list") + f"?meeting_room={m1.uuid}")
    assert resp.status_code == 200

    data = resp.json()
    assert data["count"] == 2

    # check id's of the returned json, bit ugly, but it does the job,
    # probably should need some default sorting or sorting in filter
    sorted_results = sorted(data["results"], key=lambda x: x["uuid"])
    assert list(map(lambda x: x["uuid"], sorted_results)) == sorted(
        [str(r1.uuid), str(r3.uuid)]
    )


def test_filter_reservation_by_meeting_room_name(auth_client):
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    e3 = EmployeeFactory()
    m1 = MeetingRoomFactory()
    m2 = MeetingRoomFactory()
    r1 = ReservationFactory.create(employees=(e1, e2), meeting_room=m1)
    _ = ReservationFactory.create(employees=(e1, e3), meeting_room=m2)
    r3 = ReservationFactory.create(employees=(e2, e3), meeting_room=m1)

    # filter reservations for employee #2, which should return reservations #1 and #3
    resp = auth_client.get(
        reverse("app:reservation-list") + f"?meeting_room_name={m1.name}"
    )
    assert resp.status_code == 200

    data = resp.json()
    assert data["count"] == 2

    # check id's of the returned json, bit ugly, but it does the job,
    # probably should need some default sorting or sorting in filter
    sorted_results = sorted(data["results"], key=lambda x: x["uuid"])
    assert list(map(lambda x: x["uuid"], sorted_results)) == sorted(
        [str(r1.uuid), str(r3.uuid)]
    )


def test_reservation_time_validation(auth_client):
    m1 = MeetingRoomFactory()
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    resp = auth_client.post(
        reverse("app:reservation-list"),
        {
            "title": "sprint planning",
            "from_dt": datetime.datetime(2020, 3, 14, hour=16, minute=15),
            "until_dt": datetime.datetime(2020, 3, 14, hour=15, minute=45),
            "meeting_room": m1.uuid,
            "employees": [e1.uuid, e2.uuid],
        },
    )
    assert resp.status_code == 400
    assert resp.json()["non_field_errors"] == [
        "Reservation cannot end before it begins."
    ]


def test_reservation_meeting_room_slot_validation_before(auth_client):
    # tests meeting room slot validation when posted reservation starts before other reservation,
    # but ends during it
    m1 = MeetingRoomFactory()
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    ReservationFactory(
        meeting_room=m1,
        from_dt=datetime.datetime(2020, 3, 14, hour=16, minute=30),
        until_dt=datetime.datetime(2020, 3, 14, hour=17, minute=00),
    )
    resp = auth_client.post(
        reverse("app:reservation-list"),
        {
            "title": "sprint planning",
            "from_dt": datetime.datetime(2020, 3, 14, hour=16, minute=15),
            "until_dt": datetime.datetime(2020, 3, 14, hour=16, minute=45),
            "meeting_room": m1.uuid,
            "employees": [e1.uuid, e2.uuid],
        },
    )
    assert resp.status_code == 400
    assert resp.json()["non_field_errors"] == [
        "Reservation cannot use already reserver time slot in the same meeting room"
    ]


def test_reservation_meeting_room_slot_validation_after(auth_client):
    # tests meeting room slot validation when posted reservation ends after other reservation,
    # but starts during it
    m1 = MeetingRoomFactory()
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    ReservationFactory(
        meeting_room=m1,
        from_dt=datetime.datetime(2020, 3, 14, hour=16, minute=00),
        until_dt=datetime.datetime(2020, 3, 14, hour=16, minute=30),
    )
    resp = auth_client.post(
        reverse("app:reservation-list"),
        {
            "title": "sprint planning",
            "from_dt": datetime.datetime(2020, 3, 14, hour=16, minute=15),
            "until_dt": datetime.datetime(2020, 3, 14, hour=16, minute=45),
            "meeting_room": m1.uuid,
            "employees": [e1.uuid, e2.uuid],
        },
    )
    assert resp.status_code == 400
    assert resp.json()["non_field_errors"] == [
        "Reservation cannot use already reserver time slot in the same meeting room"
    ]


def test_reservation_meeting_room_slot_validation_inside(auth_client):
    # tests meeting room slot validation when posted reservation is within other reservation's slot
    m1 = MeetingRoomFactory()
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    ReservationFactory(
        meeting_room=m1,
        from_dt=datetime.datetime(2020, 3, 14, hour=16, minute=00),
        until_dt=datetime.datetime(2020, 3, 14, hour=17, minute=00),
    )
    resp = auth_client.post(
        reverse("app:reservation-list"),
        {
            "title": "sprint planning",
            "from_dt": datetime.datetime(2020, 3, 14, hour=16, minute=15),
            "until_dt": datetime.datetime(2020, 3, 14, hour=16, minute=45),
            "meeting_room": m1.uuid,
            "employees": [e1.uuid, e2.uuid],
        },
    )
    assert resp.status_code == 400
    assert resp.json()["non_field_errors"] == [
        "Reservation cannot use already reserver time slot in the same meeting room"
    ]


def test_reservation_meeting_room_slot_validation_outside(auth_client):
    # tests meeting room slot validation when posted reservation starts before other reservation
    # begins and ends after that reservation ends;
    m1 = MeetingRoomFactory()
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    ReservationFactory(
        meeting_room=m1,
        from_dt=datetime.datetime(2020, 3, 14, hour=16, minute=25),
        until_dt=datetime.datetime(2020, 3, 14, hour=16, minute=35),
    )
    resp = auth_client.post(
        reverse("app:reservation-list"),
        {
            "title": "sprint planning",
            "from_dt": datetime.datetime(2020, 3, 14, hour=16, minute=15),
            "until_dt": datetime.datetime(2020, 3, 14, hour=16, minute=45),
            "meeting_room": m1.uuid,
            "employees": [e1.uuid, e2.uuid],
        },
    )
    assert resp.status_code == 400
    assert resp.json()["non_field_errors"] == [
        "Reservation cannot use already reserver time slot in the same meeting room"
    ]


def test_update_reservation_time_validation(auth_client):
    # Tests that PUT method on reservation resource is validated
    m1 = MeetingRoomFactory()
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    r1 = ReservationFactory(
        meeting_room=m1,
        employees=(e1, e2),
        from_dt=datetime.datetime(2020, 3, 14, hour=15, minute=15),
        until_dt=datetime.datetime(2020, 3, 14, hour=16, minute=15),
    )
    resp = auth_client.put(
        reverse("app:reservation-detail", kwargs={"pk": str(r1.uuid)}),
        {
            "title": "sprint planning",
            "from_dt": datetime.datetime(2020, 3, 14, hour=16, minute=15),
            "until_dt": datetime.datetime(2020, 3, 14, hour=15, minute=15),
            "meeting_room": m1.uuid,
            "employees": [e1.uuid, e2.uuid],
        },
    )
    assert resp.status_code == 400
    assert resp.json()["non_field_errors"] == [
        "Reservation cannot end before it begins."
    ]


def test_partial_update_reservation_time_validation(auth_client):
    # Tests that PATCH method on reservation resource is validated
    m1 = MeetingRoomFactory()
    e1 = EmployeeFactory()
    e2 = EmployeeFactory()
    r1 = ReservationFactory(
        meeting_room=m1,
        employees=(e1, e2),
        from_dt=datetime.datetime(2020, 3, 14, hour=15, minute=15),
        until_dt=datetime.datetime(2020, 3, 14, hour=16, minute=15),
    )
    resp = auth_client.patch(
        reverse("app:reservation-detail", kwargs={"pk": str(r1.uuid)}),
        {
            "title": "sprint planning",
            "from_dt": datetime.datetime(2020, 3, 14, hour=16, minute=15),
            "until_dt": datetime.datetime(2020, 3, 14, hour=15, minute=15),
            "meeting_room": m1.uuid,
            "employees": [e1.uuid, e2.uuid],
        },
    )
    assert resp.status_code == 400
    assert resp.json()["non_field_errors"] == [
        "Reservation cannot end before it begins."
    ]


def test_get_employees_no_auth(client):
    EmployeeFactory()
    EmployeeFactory()
    resp = client.get(reverse("app:employee-list"))
    assert resp.status_code == 401


def test_get_employees(auth_client):
    EmployeeFactory()
    EmployeeFactory()
    resp = auth_client.get(reverse("app:employee-list"))
    assert resp.status_code == 200
    assert resp.json()["count"] == 3
