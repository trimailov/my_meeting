# My Meeting

A REST API demo application for meeting room scheduling

## Table of contents

- [Demo](#demo)
- [Features](#features)
- [How to use API](#how-to-use-api)
- [How to develop](#how-to-develop):
    - [Local development with Docker](#local-dev-in-docker)
    - [Local development without Docker](#local-dev):
        - [Issues with psycopg2](#issue-w-psycopg2)
    - [Requirements](#requirements)
    - [Pre-commit hooks](#pre-commit-hooks)
- [Best practices](#best-practices)
- [API endpoint examples with curl](#api-endpoint-examples)

## <a name="demo"></a>Demo

Demo application is deployed and available at heroku: [https://my-meeting.herokuapp.com](https://my-meeting.herokuapp.com/)

This demo app is available with HTTPS, thus your passwords should be safe(r) from vilanous eyes.

Visitors are welcome to try it out. How to use this API is listed [below](#how-to-use-api).

App may be slow to respond for the first request, due heroku sending the app to sleep state.

## <a name="features"></a>Features

- Authentication:
    - Username and passowrd to authenticate and receive an API token;
    - Most endpoints require token for authorization;
    - Token invalidation endpoint (logout);
- Users/Employees:
    - Create user and list all existing users;
- Meeting rooms:
    - Create/Retrieve/Update/Delete meeting rooms;
    - Check meeting room availability by filtering reservations by meeting room uuid or name;
- Meeting room reservations:
    - Create/Retrieve/Update/Delete reservations;
    - Validation to not create reservation on top of existing meeting room reservation;
    - Separate endpoint to cancel reservation;
    - Filter reservations by:
        - `employees` - employee uuid, multiple allowed;
        - `usernames` - employee username, multiple allowed;
        - `canceled` - boolean (`true/false`);
        - `meeting_room` - meeting_room uuid;
        - `meeting_room_name` - meeting_room name;
- Pagination:
    - List views return 10 results;
    - Json response have `next` and `prev` keys with links to next and previous pages;

## <a name="how-to-use-api"></a>How to use the API

Here is listed all available REST API endpoints. Each endpoint has example usage linked to examples below.

All API endpoints (except login and create user) require authorization token in request headers, e.g.: `Authorization: Token <my_token>`.

Typical use:
- Create your user: `POST /auth/users/`;
- Login: `POST /auth/token/login`;
- Create meeting rooms if necessary: `POST /api/v1/meeting-rooms/`;
- Create reservations: `POST /api/v1/reservations/`;
- Retrieve/Update/Delete reservations (examples at [API examples](#api-endpoint-examples));
- Logout: `POST /auth/token/logout`;

| Method | Endpoint                                                               | Description                             | Status |
| ------ | ---------------------------------------------------------------------- | --------------------------------------- | ------ |
| POST   | [`/auth/users/`](#create-user)                                         | Create user                             | 201    |
| GET    | [`/auth/users/me/`](#get-user)                                         | Get current user information            | 200    |
| POST   | [`/auth/token/login/`](#login)                                         | Create API token using user credenitals | 201    |
| POST   | [`/auth/token/logout/`](#logout)                                       | Invalidate user's API token             | 204    |
| GET    | [`/api/v1/employees/`](#get-employees)                                 | Get user (aka employee) list            | 200    |
| POST   | [`/api/v1/meeting-rooms/`](#create-meeting-room)                       | Create meeting room                     | 201    |
| PUT    | [`/api/v1/meeting-rooms/<uuid>/`](#put-meeting-room)                   | Put meeting room                        | 200    |
| PATCH  | [`/api/v1/meeting-rooms/<uuid>/`](#patch-meeting-room)                 | Patch meeting room                      | 200    |
| GET    | [`/api/v1/meeting-rooms/`](#get-meeting-rooms)                         | Get all meeting rooms                   | 200    |
| GET    | [`/api/v1/meeting-rooms/<uuid>/`](#get-meeting-room)                   | Get a meeting room                      | 200    |
| DELETE | [`/api/v1/meeting-rooms/<uuid>/`](#delete-meeting-room)                | Delete meeting room                     | 204    |
| POST   | [`/api/v1/reservations/`](#create-reservation)                         | Create reservation                      | 201    |
| PUT    | [`/api/v1/reservations/<uuid>/`](#put-reservation)                     | Put reservation                         | 200    |
| PATCH  | [`/api/v1/reservations/<uuid>/`](#patch-reservation)                  | Patch reservation                       | 200    |
| GET    | [`/api/v1/reservations/`](#get-reservations)                           | Get all reservations                    | 200    |
| GET    | [`/api/v1/reservations/?usernames=foobar`](#get-filtered-reservations) | Get filtered reservations               | 200    |
| GET    | [`/api/v1/reservations/<uuid>/`](#get-reservation)                     | Get a reservation                       | 200    |
| PATCH  | [`/api/v1/reservations/<uuid>/cancel/`](#cancel-reservation)           | Cancel reservation                      | 200    |
| DELETE | [`/api/v1/reservations/<uuid>/`](#delete-reservation)                  | Delete reservation                      | 204    |

## <a name="how-to-develop"></a>How to develop

On your machine you should have:
- [Docker](https://docs.docker.com/get-started/#download-and-install-docker-desktop);
- [docker-compose](https://docs.docker.com/compose/install/);
- Python 3.8.6 (If want to develop on local machine, otherwise `Docker` is enough)

### <a name="local-dev-in-docker"></a>Local development in Docker

There's `Dockerfile.dev` and `docker-compose.dev.yml` for development purposes.

To build, run and develop app using docker run:

```
docker-compose -f docker-compose.dev.yml up --build
```

To run commands on the app container:

```
docker-compose -f docker-compose.dev.yml exec app <command> <arg1> <option2>
```

Application should be reachable at [http://localhost:8000/](http://localhost:8000/)

### <a name="local-dev"></a>Local development without Docker

I usually prefer to run the developed code on local machine and launch only other services with docker.

To build environment for web server development on local machine:

```
# creates python virtual environment and installs requirements
make
```

To run:

```
# make sure postgres service is running
docker-compose -f docker-compose.dev.yml up db

# builds environment (if necessary) and runs the web server
make run
```

Application should be reachable at [http://localhost:8000/](http://localhost:8000/)

#### <a name="issue-w-psycopg2"></a>Issues with psycopg2 and local build

`psycopg2` may not install on your local machine because it cannot find `openssl`.

You need to set `LDFLAGS` and `CPPFLAGS` env variable to openssl `lib` and `include` dirs.
In my case (using brew on macos) it was `"-L/usr/local/opt/openssl/lib"` and `"-L/usr/local/opt/openssl/include"` respectively.

### <a name="requirements"></a>Requirements

Edit `requirements.in` or `requirements-dev.in` to change required packages.

To build `requirements*.txt` files:

```
make requirements
```

To upgrade package versions in `requirements*.txt` files:

```
make upgrade-requirements
```

### <a name="pre-commit-hooks"></a>Pre-commit hooks

To avoid code quality issues, contributors are encouraged to use [pre-commit hooks](https://pre-commit.com/#intro).
Otherwise linters and formatters may not allow pipelines to pass.

There are multiple ways to install `pre-commit` hooks:

- If you've build local dev environment using `make`:
```
env/bin/pre-commit install
env/bin/pre-commit install-hooks
```

- If you have not run `make` yet and develop in `docker` only, I still encourage to install `pip-tools` in virtual environment, to do so run:
```
# create virtual environment for python
make env/bin/pip

# and then repeat same pre-commit install commands:
env/bin/pre-commit install
env/bin/pre-commit install-hooks
```

- Otherwise install `pre-commit` using any way convenient for you (e.g. global `pip`) and run same commands as above:
```
pre-commit install
pre-commit install-hooks
```

## <a name="best-pracitces"></a>Best practices

This demo project tries to use various best practices:
- Version control in git;
- Continuous integration to lint, check code formatting and automate tests;
- Continuous deployment to build Docker image and deploy to Heroku;
- Local development in python virtual environment with `make`;
- Alternatively - local development in Docker container with `docker-compose`;
- Dependent services run in Docker (e.g. Postgres);
- Automated tests with code coverage checking and tracking in CI analytics;
- Deployment secrets are stored in CI/CD environment and are not exposed in version control or any other non-authorized way;
- Middleware to log some details on requests (response time, user id, status, endpoint, query, etc.) to console;
- Using Gitlab issue tracking for feature implementations;
- Using merge requests to develop features and fix issues without poluting master branch with possibly broken state;

Known issues and shortcomings:
- This demo application assumes it's use by one company only;
    - Either `company` model is necessary or this application should be accessible through private VPN;
- Continuous deployment does not have automated management commands (e.g. migrations);
- Logging:
    - could use something like graylog service, but it's deployment and management is out of scope of a small demo;
    - Heroku has efemeral filesystem and limited console length so logs are not persistant;
- Tests should run on final wheel or docker image:
    - Though this setup is little bit more complex and not implemented yet, due to time constraints;
- Would be nice to automate API documentation with swagger, but not done yet, due to time constraints;

## <a name="api-endpoint-examples"></a>API endpoint examples with curl

<a name="create-user"></a>Create user:
```bash
curl -X POST https://my-meeting.herokuapp.com/auth/users/ --data "username=foobar&password=letmetest"
{"email":"","username":"foobar","uuid":"7a49fb03-4b1f-457e-95fa-8577fee04f3b"}
```

<a name="get-user"></a>Get current user info:
```bash
curl -LX GET https://my-meeting.herokuapp.com/auth/users/me/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
{
    "email": "",
    "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
    "username": "foobar"
}
```

<a name="login"></a>Login (create user token):
```bash
curl -X POST https://my-meeting.herokuapp.com/auth/token/login --data "username=foobar&password=letmetest"
{"auth_token":"5b482adae3c0dbe9875c6c5992cd3898a78c0a6c"}
```

<a name="logout"></a>Logout (invalidate token):
```bash
curl -X POST https://my-meeting.herokuapp.com/auth/token/logout/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c"

curl -X GET https://my-meeting.herokuapp.com/auth/users/me/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c"
{"detail":"Invalid token."}
```

<a name="get-employees"></a>Get all employees:
```bash
curl -LX GET https://my-meeting.herokuapp.com/api/v1/employees/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
            "username": "justas"
        },
        {
            "uuid": "ea02480f-a858-4cca-ae1c-68f12c9afcb6",
            "username": "admin"
        },
        {
            "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
            "username": "foobar"
        }
    ]
}
```

<a name="create-meeting-room"></a>Create a meeting room:
```bash
curl -X POST https://my-meeting.herokuapp.com/api/v1/meeting-rooms/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4" \
  --data "name=conf room"
{
    "uuid": "255783e3-2051-4367-aa56-fad6b04d33f0",
    "name": "conf room"
}
```

<a name="put-meeting-room"></a>Put a meeting room:
```bash
curl -LX PUT "https://my-meeting.herokuapp.com/api/v1/meeting-rooms/8aba71ff-5490-44ee-8519-d112943db972/" \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4" \
  --data "name=big conf room"
{
    "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
    "name": "big conf room"
}
```

<a name="patch-meeting-room"></a>Patch a meeting room:
```bash
curl -LX PATCH "https://my-meeting.herokuapp.com/api/v1/meeting-rooms/8aba71ff-5490-44ee-8519-d112943db972/" \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4" \
  --data "name=conf room"
{
    "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
    "name": "conf room"
}
```

<a name="get-meeting-rooms"></a>Get all meeting rooms:
```bash
curl -LX GET https://my-meeting.herokuapp.com/api/v1/meeting-rooms/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "uuid": "255783e3-2051-4367-aa56-fad6b04d33f0",
            "name": "conf room"
        },
        {
            "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
            "name": "klas room"
        },
        {
            "uuid": "ffeb18f5-b5e3-4cad-b186-ced7fda7ad8e",
            "name": "smol room"
        }
    ]
}
```

<a name="get-meeting-room"></a>Get a meeting room:
```bash
curl -LX GET https://my-meeting.herokuapp.com/api/v1/meeting-rooms/255783e3-2051-4367-aa56-fad6b04d33f0/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
{
    "uuid": "255783e3-2051-4367-aa56-fad6b04d33f0",
    "name": "conf room"
}
```

<a name="delete-meeting-room"></a>Delete a meeting room:
```bash
curl -X DELETE https://my-meeting.herokuapp.com/api/v1/meeting-rooms/255783e3-2051-4367-aa56-fad6b04d33f0/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
```

<a name="create-reservation"></a>Create reservation:
```bash
curl -X POST https://my-meeting.herokuapp.com/api/v1/reservations/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4" \
  -H "Content-Type: application/json" \
  --data '{"title": "sprint planning", "from_dt": "2020-10-23T10:00", "until_dt": "2020-10-23T11:00", "meeting_room": "8aba71ff-5490-44ee-8519-d112943db972", "employees": ["7a49fb03-4b1f-457e-95fa-8577fee04f3b", "be9c9373-3d2a-4cfe-8d42-5ae66728963c"]}'
{
    "uuid": "4e84efa2-9999-4b73-b4bb-6e498014e8b4",
    "title": "sprint planning",
    "from_dt": "2020-10-23T10:00:00Z",
    "until_dt": "2020-10-23T11:00:00Z",
    "created_by": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
    "meeting_room": "8aba71ff-5490-44ee-8519-d112943db972",
    "employees": [
        "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
        "7a49fb03-4b1f-457e-95fa-8577fee04f3b"
    ],
    "canceled": false
}
```


<a name="put-reservation"></a>Put a reservation:
```bash
curl -LX PUT "https://my-meeting.herokuapp.com/api/v1/reservations/10b5bfba-33d1-4366-a473-4c754525ceb1/" \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4" \
  --data "canceled=false&title=agile planning&from_dt=2020-10-24T11:20:00&until_dt=2020-10-24T12:20:00"
{
    "uuid": "10b5bfba-33d1-4366-a473-4c754525ceb1",
    "title": "agile planning",
    "from_dt": "2020-10-24T11:20:00Z",
    "until_dt": "2020-10-24T12:20:00Z",
    "created_by": {
        "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
        "username": "foobar"
    },
    "meeting_room": {
        "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
        "name": "klas room"
    },
    "employees": [
        {
            "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
            "username": "justas"
        },
        {
            "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
            "username": "foobar"
        }
    ],
    "canceled": false
}
```

<a name="patch-reservation"></a>Patch a reservation:
```bash
curl -LX PATCH "https://my-meeting.herokuapp.com/api/v1/reservations/10b5bfba-33d1-4366-a473-4c754525ceb1/" \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4" \
  --data "canceled=false"
{
    "uuid": "10b5bfba-33d1-4366-a473-4c754525ceb1",
    "title": "backlog grooming",
    "from_dt": "2020-10-23T11:20:00Z",
    "until_dt": "2020-10-23T12:20:00Z",
    "created_by": {
        "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
        "username": "foobar"
    },
    "meeting_room": {
        "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
        "name": "klas room"
    },
    "employees": [
        {
            "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
            "username": "justas"
        },
        {
            "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
            "username": "foobar"
        }
    ],
    "canceled": false
}
```

<a name="get-reservations"></a>Get all reservations:
```bash
curl -X GET https://my-meeting.herokuapp.com/api/v1/reservations/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "uuid": "4e84efa2-9999-4b73-b4bb-6e498014e8b4",
            "title": "sprint planning",
            "from_dt": "2020-10-23T10:00:00Z",
            "until_dt": "2020-10-23T11:00:00Z",
            "created_by": {
                "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                "username": "foobar"
            },
            "meeting_room": {
                "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
                "name": "klas room"
            },
            "employees": [
                {
                    "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
                    "username": "justas"
                },
                {
                    "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                    "username": "foobar"
                }
            ],
            "canceled": false
        },
        {
            "uuid": "4d608705-1be7-48b0-9b82-3c2a9e41ec5c",
            "title": "backlog grooming",
            "from_dt": "2020-10-23T11:00:00Z",
            "until_dt": "2020-10-23T12:00:00Z",
            "created_by": {
                "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                "username": "foobar"
            },
            "meeting_room": {
                "uuid": "ffeb18f5-b5e3-4cad-b186-ced7fda7ad8e",
                "name": "smol room"
            },
            "employees": [
                {
                    "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
                    "username": "justas"
                },
                {
                    "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                    "username": "foobar"
                }
            ],
            "canceled": false
        },
        {
            "uuid": "10b5bfba-33d1-4366-a473-4c754525ceb1",
            "title": "backlog grooming",
            "from_dt": "2020-10-23T11:20:00Z",
            "until_dt": "2020-10-23T12:20:00Z",
            "created_by": {
                "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                "username": "foobar"
            },
            "meeting_room": {
                "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
                "name": "klas room"
            },
            "employees": [
                {
                    "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
                    "username": "justas"
                },
                {
                    "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                    "username": "foobar"
                }
            ],
            "canceled": false
        }
    ]
}
```

<a name="get-filtered-reservations"></a>Get filtered reservations:

These filters exist:
- `employees`: filter by multiple uuids of employees;
- `usernames`: filter by multiple usernames of employees;
- `canceled`: filter by cancelation attribute
- `meeting_room`: filter by meeting_room uuid;
- `meeting_room_name`: filter by meeting_room name;

For example:

```bash
curl -LX GET "https://my-meeting.herokuapp.com/api/v1/reservations/?usernames=justas&usernames=foobar&canceled=false" \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "uuid": "4e84efa2-9999-4b73-b4bb-6e498014e8b4",
            "title": "sprint planning",
            "from_dt": "2020-10-23T10:00:00Z",
            "until_dt": "2020-10-23T11:00:00Z",
            "created_by": {
                "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                "username": "foobar"
            },
            "meeting_room": {
                "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
                "name": "klas room"
            },
            "employees": [
                {
                    "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
                    "username": "justas"
                },
                {
                    "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                    "username": "foobar"
                }
            ],
            "canceled": false
        },
        {
            "uuid": "4d608705-1be7-48b0-9b82-3c2a9e41ec5c",
            "title": "backlog grooming",
            "from_dt": "2020-10-23T11:00:00Z",
            "until_dt": "2020-10-23T12:00:00Z",
            "created_by": {
                "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                "username": "foobar"
            },
            "meeting_room": {
                "uuid": "ffeb18f5-b5e3-4cad-b186-ced7fda7ad8e",
                "name": "smol room"
            },
            "employees": [
                {
                    "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
                    "username": "justas"
                },
                {
                    "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
                    "username": "foobar"
                }
            ],
            "canceled": false
        }
    ]
}
```

<a name="get-reservation"></a>Get a reservation:
```bash
curl -X GET https://my-meeting.herokuapp.com/api/v1/reservations/10b5bfba-33d1-4366-a473-4c754525ceb1/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
{
    "uuid": "10b5bfba-33d1-4366-a473-4c754525ceb1",
    "title": "backlog grooming",
    "from_dt": "2020-10-23T11:20:00Z",
    "until_dt": "2020-10-23T12:20:00Z",
    "created_by": {
        "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
        "username": "foobar"
    },
    "meeting_room": {
        "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
        "name": "klas room"
    },
    "employees": [
        {
            "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
            "username": "justas"
        },
        {
            "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
            "username": "foobar"
        }
    ],
    "canceled": false
}
```

<a name="cancel-reservation"></a>Cancel reservation:
```bash
curl -X PATCH https://my-meeting.herokuapp.com/api/v1/reservations/10b5bfba-33d1-4366-a473-4c754525ceb1/cancel/ \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
{
    "uuid": "10b5bfba-33d1-4366-a473-4c754525ceb1",
    "title": "backlog grooming",
    "from_dt": "2020-10-23T11:20:00Z",
    "until_dt": "2020-10-23T12:20:00Z",
    "created_by": {
        "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
        "username": "foobar"
    },
    "meeting_room": {
        "uuid": "8aba71ff-5490-44ee-8519-d112943db972",
        "name": "klas room"
    },
    "employees": [
        {
            "uuid": "be9c9373-3d2a-4cfe-8d42-5ae66728963c",
            "username": "justas"
        },
        {
            "uuid": "7a49fb03-4b1f-457e-95fa-8577fee04f3b",
            "username": "foobar"
        }
    ],
    "canceled": true
}
```

<a name="delete-reservation"></a>Delete reservation:
```bash
curl -X DELETE "https://my-meeting.herokuapp.com/api/v1/reservations/4e84efa2-9999-4b73-b4bb-6e498014e8b4/" \
  -H "Authorization: Token 5b482adae3c0dbe9875c6c5992cd3898a78c0a6c" \
  -H "Accept: application/json; indent=4"
```
