import http.client
import logging
import os
import time
import uuid

from ipware import get_client_ip
from structlog import wrap_logger

logger = wrap_logger(logger=logging.getLogger("django.request"))


class RequestLoggingMiddleware:
    """
    Log incoming requests and specified non-Django/non-DRF exceptions.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        start_time = time.time() * 1000  # in milliseconds

        request_id = request.META.get("HTTP_X_REQUEST_ID", uuid.uuid4().hex)
        client_ip, _ = get_client_ip(request)

        logging_context = dict(
            host=request.get_host().split(":")[0],
            port=request.get_port(),
            method=request.method,
            path=request.path,
            query_params=request.GET.urlencode(),
            remote_addr=client_ip or None,
            request_id=request_id,
            pid=os.getpid(),
        )

        try:
            response = self.get_response(request)
        except Exception as exc:
            end_time = time.time() * 1000
            request_time = int(end_time - start_time)
            self.add_user_context(request, logging_context)
            logger.error(
                "request_error",
                request_time=request_time,
                status_code=500,
                message=str(exc),
                exc_info=True,
                extra=logging_context,
            )
            raise
        else:
            end_time = time.time() * 1000
            request_time = int(end_time - start_time)
            self.add_user_context(request, logging_context)
            logger.info(
                "request_info",
                request_time=request_time,
                status_code=response.status_code,
                message=http.client.responses.get(response.status_code, ""),
                extra=logging_context,
            )

        return response

    def process_exception(self, request, exception):
        logger.error("request_error", message=str(exception), exc_info=True)

    def add_user_context(self, request, logging_context):
        user_context = {}
        if hasattr(request, "user") and hasattr(request.user, "uuid"):
            user_context = {
                "user_uuid": str(request.user.uuid),
                "user_username": request.user.username,
            }
        logging_context.update(user_context)
