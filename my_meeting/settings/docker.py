from my_meeting.settings.base import *  # noqa

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "my_meeting",
        "USER": "admin",
        "PASSWORD": "secret",
        "HOST": "db",
        "PORT": "5432",
    }
}
