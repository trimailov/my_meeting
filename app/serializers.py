from django.db.models import Q
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from app.models import Employee, MeetingRoom, Reservation


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        read_only_fields = ("uuid",)
        fields = ("uuid", "username")


class MeetingRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeetingRoom
        read_only_fields = ("uuid",)
        fields = ("uuid", "name")


class ReservationCreateSerializer(serializers.ModelSerializer):
    created_by = serializers.PrimaryKeyRelatedField(
        default=CurrentUserDefault(),
        queryset=Employee.objects.all(),
    )

    class Meta:
        model = Reservation
        read_only_fields = ("uuid",)
        fields = (
            "uuid",
            "title",
            "from_dt",
            "until_dt",
            "created_by",
            "meeting_room",
            "employees",
            "canceled",
        )

    def validate(self, data):
        if data["from_dt"] > data["until_dt"]:
            raise serializers.ValidationError(
                "Reservation cannot end before it begins."
            )

        # Select reservations which occupy same time slot we are trying to take at the
        # same meeting room.
        # We need to query reservations which are in the same room
        # AND
        # filter for reservations where our start time is crossing other reservations time slot
        # OR
        # filter for reservations where our end time is crossing other reservations time slot
        # OR
        # filter for reservations where our start time is before and end time is after other
        # reservations time slot
        crossing_reservations_count = Reservation.objects.filter(
            Q(meeting_room=data["meeting_room"]),
            Q(from_dt__lt=data["from_dt"], until_dt__gt=data["from_dt"])
            | Q(from_dt__lt=data["until_dt"], until_dt__gt=data["until_dt"])
            | Q(from_dt__gt=data["from_dt"], until_dt__lt=data["until_dt"]),
        ).count()
        if crossing_reservations_count > 0:
            raise serializers.ValidationError(
                "Reservation cannot use already reserver time slot in the same meeting room"
            )

        return data


class ReservationSerializer(serializers.ModelSerializer):
    created_by = EmployeeSerializer(read_only=True)
    employees = EmployeeSerializer(many=True, read_only=True)
    meeting_room = MeetingRoomSerializer(read_only=True)

    class Meta:
        model = Reservation
        read_only_fields = ("uuid",)
        fields = (
            "uuid",
            "title",
            "from_dt",
            "until_dt",
            "created_by",
            "meeting_room",
            "employees",
            "canceled",
        )
