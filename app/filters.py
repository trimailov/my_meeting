from django_filters import rest_framework as filters

from app.models import Employee, MeetingRoom, Reservation


class ReservationFilter(filters.FilterSet):
    usernames = filters.ModelMultipleChoiceFilter(
        field_name="employees__username",
        to_field_name="username",
        queryset=Employee.objects.all(),
    )
    meeting_room_name = filters.ModelChoiceFilter(
        field_name="meeting_room",  # XXX: why this notation is different from above?
        to_field_name="name",
        queryset=MeetingRoom.objects.all(),
    )

    class Meta:
        model = Reservation
        fields = ("employees", "canceled", "meeting_room")
