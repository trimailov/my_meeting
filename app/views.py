from rest_framework import generics, permissions, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from app.filters import ReservationFilter
from app.models import Employee, MeetingRoom, Reservation
from app.serializers import (
    EmployeeSerializer,
    MeetingRoomSerializer,
    ReservationCreateSerializer,
    ReservationSerializer,
)


class EmployeeListView(generics.ListAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = [permissions.IsAuthenticated]


class MeetingRoomViewSet(viewsets.ModelViewSet):
    queryset = MeetingRoom.objects.all().order_by("name")
    serializer_class = MeetingRoomSerializer
    permission_classes = [permissions.IsAuthenticated]


class ReservationViewSet(viewsets.ModelViewSet):
    queryset = Reservation.objects.all().order_by("from_dt", "uuid")
    permission_classes = [permissions.IsAuthenticated]
    filterset_class = ReservationFilter

    def get_serializer_class(self):
        if self.action in ("create", "update", "partial_update"):
            return ReservationCreateSerializer
        return ReservationSerializer

    @action(["patch"], detail=True)
    def cancel(self, request, pk=None):
        reservation = self.get_object()
        reservation.cancel()
        reservation.save()
        serializer = ReservationSerializer(reservation)
        return Response(serializer.data)
