import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models


class Employee(AbstractUser):
    uuid = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    full_name = models.CharField(max_length=100)


class MeetingRoom(models.Model):
    uuid = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=100, unique=True)


class Reservation(models.Model):
    uuid = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    title = models.CharField(max_length=100)
    from_dt = models.DateTimeField()
    until_dt = models.DateTimeField()
    created_by = models.ForeignKey(
        Employee, on_delete=models.CASCADE, related_name="created_reservations"
    )
    meeting_room = models.ForeignKey(MeetingRoom, on_delete=models.CASCADE)
    employees = models.ManyToManyField(Employee, related_name="reservations")
    canceled = models.BooleanField(default=False)

    def cancel(self):
        self.canceled = True
