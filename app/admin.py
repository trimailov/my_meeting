from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from app.models import Employee, MeetingRoom, Reservation


class MeetingRoomAdmin(admin.ModelAdmin):
    pass


class ReservationAdmin(admin.ModelAdmin):
    pass


admin.site.register(Employee, UserAdmin)
admin.site.register(MeetingRoom, MeetingRoomAdmin)
admin.site.register(Reservation, ReservationAdmin)
