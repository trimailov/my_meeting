from django.urls import include, path
from rest_framework import routers

from app import views

router = routers.DefaultRouter()
router.register(r"meeting-rooms", views.MeetingRoomViewSet)
router.register(r"reservations", views.ReservationViewSet)


app_name = "app"


urlpatterns = [
    path("", include(router.urls)),
    path("employees/", views.EmployeeListView.as_view(), name="employee-list"),
]
