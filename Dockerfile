FROM python:3.8.6-buster

COPY . /my_meeting
WORKDIR /my_meeting
RUN pip3 install --no-cache-dir -q -r requirements.txt

RUN adduser --disabled-password --gecos "" my_meeting_user
USER my_meeting_user

ARG postgres_db
ARG postgres_user
ARG postgres_password
ARG postgres_host
ARG secret_key

ENV DJANGO_SETTINGS_MODULE=my_meeting.settings.heroku \
    POSTGRES_DB=$postgres_db \
    POSTGRES_USER=$postgres_user \
    POSTGRES_PASSWORD=$postgres_password \
    POSTGRES_HOST=$postgres_host \
    SECRET_KEY=$secret_key

RUN python manage.py collectstatic

CMD gunicorn --bind 0.0.0.0:$PORT my_meeting.wsgi
